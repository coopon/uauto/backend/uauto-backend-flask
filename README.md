# uauto-backend-flask

**Requirements**
1. pipenv (sudo -H pip3 install pipenv)

**Before Starting the App**
1. pipenv install
2. pipenv shell
3. flask db upgrade

**Starting the App**
1. flask run

## LICENSE
AGPLv3

## DATABASE

### DEFAULT

database - uautodb
password - testpw
user - postgres

### .env FILE VARIABLE

DATABASE_URI

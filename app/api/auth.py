from functools import wraps

from flask import g, request
from flask_httpauth import HTTPTokenAuth

from app.models import Driver, Passenger
from app.api.errors import error_response, bad_request

token_auth = HTTPTokenAuth()
accepted_values = ["driver", "passenger"]

def login_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        if "X-Request-From" not in request.headers:
            return bad_request("Missing X-Request-From header")

        required_fields = ["mobile", "password"]
        data = request.get_json() or {}

        for field in required_fields:
            if field not in data:
                return bad_request("{} is required".format(field))

        _mobile = data.get('mobile')
        _password = data.get('password')
        _entity = request.headers['X-Request-From'].lower()

        if _entity not in accepted_values:
            return bad_request("{} is not a valid entity".format(_entity))

        if not verify_password(_mobile, _password, _entity):
            return error_callback()
        
        return f(*args, **kwargs)
    return decorated

def verify_password(mobile, password, entity):
    if entity == "driver":
        user = Driver.query.filter_by(mobile=mobile).first()
    elif entity == "passenger":
        user = Passenger.query.filter_by(mobile=mobile).first()

    if user is None:
        return False

    g.current_user = user
    return user.check_password(password)

def error_callback():
    return error_response(401)

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        required_headers = ["Authorization", "X-Request-From"]

        for field in required_headers:
            if field not in request.headers:
                return bad_request("Missing {} header".format(field))

        auth_type, token = request.headers['Authorization'].split(None, 1)
        if auth_type != "Bearer":
            return bad_request("Only Bearer type is supported")

        _entity = request.headers['X-Request-From'].lower()
        if _entity not in accepted_values:
            return bad_request("{} is not a valid entity".format(_entity))

        if not verify_token(token, _entity):
            return error_callback()

        return f(*args, **kwargs)
    return decorated

def verify_token(token, entity):
    if entity == "driver":
        user = Driver.check_token(token) if token else None
    elif entity == "passenger":
        user = Passenger.check_token(token) if token else None

    if user is None:
        return False

    g.current_user = user
    return g.current_user is not None

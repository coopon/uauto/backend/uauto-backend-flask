from flask import jsonify, request
from sqlalchemy import exc

from app import db
from app.api import bp
from app.models import Ride
from app.enums import RideStatus
from app.utils import to_dict, from_dict
from app.api.errors import bad_request, server_error
from app.api.auth import token_required

''' retreive a particular ride details '''
@bp.route('/rides/<int:r_id>', methods=['GET'])
@token_required
def get_ride(r_id):
    ride = Ride.query.get_or_404(r_id)
    ignore = []
    return jsonify(to_dict(ride, ignore))


''' handle new ride request '''
@bp.route('/rides', methods=['POST'])
@token_required
def new_ride_request():
    data = request.get_json() or {}
    required_fields = ["start_location", "dest_location"]

    for field in required_fields:
        if field not in data:
            return bad_request("{} is required".format(field))

    ride = Ride()
    ride.status = RideStatus.USER_REQUESTED

    # TODO: update based on token
    ride.passenger_id = 2

    from_dict(data, ride)

    try:
        db.session.add(ride)
        db.session.commit()
    except Exception as ex:
        print ("Exception Occurred: {}".format(ex))
        return server_error("something went wrong on API side")

    response = jsonify(to_dict(ride, ignore=[]))
    response.status_code = 201
    return response


''' update ride details '''
@bp.route('/rides/<int:r_id>', methods=['PUT'])
@token_required
def update_ride(r_id):
    ride = Ride.query.get_or_404(r_id)
    data = request.get_json() or {}

    dont_allow = ["r_id", "start_location", "dest_location", "estimated_dist", "estimated_cost", "passenger_id", "started_at"]
    for field in dont_allow:
        if field in data:
            data.pop(field)

    if "status" in data:
        _status = data.pop("status").upper()
        if hasattr(RideStatus, _status):
            data["status"] = RideStatus[_status]
        else:
            return bad_request("unsupported ride status")

    if "driver_id" in data:
        driver = Driver.query.get(int(data.get('driver_id')))
        if not driver:
            return bad_request("invalid driver id")
        else:
            data['driver_id'] = int(data.get('driver_id'))

    from_dict(data, ride)

    try:
        db.session.commit()
    except Exception as ex:
        print ("Exception Occurred: {}".format(ex))
        return server_error("something went wrong on API side")

    return jsonify(to_dict(ride, ignore=[]))

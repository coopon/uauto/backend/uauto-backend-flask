from flask import Blueprint

bp = Blueprint('api', __name__)

from app.api import tokens
from app.api import drivers, passengers, vehicles, rides

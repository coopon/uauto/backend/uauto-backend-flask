from flask import jsonify, request, g
from sqlalchemy import exc

from app import db
from app.api import bp
from app.models import Driver, Fleet
from app.utils import to_dict, from_dict
from app.api.errors import bad_request, server_error
from app.api.auth import token_required

''' retreive a particular driver '''
@bp.route('/drivers/<int:d_id>', methods=['GET'])
@token_required
def get_driver(d_id):
    driver = Driver.query.get_or_404(d_id)
    ignore = ["password_hash", "license_no", "token", "token_expiration"]
    return jsonify(to_dict(driver, ignore))


''' create a driver '''
@bp.route('/drivers', methods=['POST'])
def create_driver():
    data = request.get_json() or {}
    required_fields = ["name", "mobile", "license_no", "password"]

    for field in required_fields:
        if field not in data:
            return bad_request("{} is required".format(field))

    driver = Driver()
    from_dict(data, driver)
    driver.set_password(data.get('password'))
    try:
        db.session.add(driver)
        db.session.commit()
    except exc.IntegrityError:
        return bad_request("driver already registered")

    response = jsonify(to_dict(driver, ignore=["password_hash", "token", "token_expiration"]))
    response.status_code = 201
    return response


''' update driver details '''
@bp.route('/drivers/<int:d_id>', methods=['PUT'])
@token_required
def update_driver(d_id):
    driver = Driver.query.get_or_404(d_id)
    data = request.get_json() or {}

    dont_allow = ["d_id", "license_no", "mobile", "email", "token", "token_expiration"]
    for field in dont_allow:
        if field in data:
            data.pop(field)

    # check if fleet_id is present
    if "fleet_id" in data:
        fleet_id = int(data.get("fleet_id"))
        # TODO: check if driver driving for and fleet_for matches
        fleet = Fleet.query.get(fleet_id)
        if not fleet:
            return bad_request("no such fleet")

    if "password" in data:
        password_plain = data.pop("password")
        driver.set_password(password_plain)

    from_dict(data, driver)
    db.session.commit()

    return jsonify(to_dict(driver, ignore=["password_hash", "token", "token_expiration"]))


''' remove a driver '''
@bp.route('/drivers/<int:d_id>', methods=['DELETE'])
@token_required
def remove_driver(d_id):
    driver = Driver.query.get(d_id)

    if driver:
        try:
            db.session.delete(driver)
            db.session.commit()
        except Exception:
            return server_error("something went wrong on API side")

    return jsonify(), 204


''' get rides for the driver '''
@bp.route('/drivers/<int:d_id>/rides', methods=['GET'])
@token_required
def get_rides(d_id):
    driver = Driver.query.get_or_404(d_id)
    rides = driver.rides

    response = {}
    response["total_rides"] = len(rides)

    if len(rides) != 0:
        driver_rides = [to_dict(ride) for ride in rides]
        response["rides"] = driver_rides
    else:
        response["rides"] = rides

    return jsonify(response)

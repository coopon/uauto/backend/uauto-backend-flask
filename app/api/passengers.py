from flask import jsonify, request
from sqlalchemy import exc

from app import db
from app.api import bp
from app.models import Passenger
from app.utils import to_dict, from_dict
from app.api.errors import bad_request, server_error
from app.api.auth import token_required

''' retreive a particular passenger '''
@bp.route('/passengers/<int:d_id>', methods=['GET'])
@token_required
def get_passenger(d_id):
    passenger = Passenger.query.get_or_404(d_id)
    ignore = ["password_hash", "token", "token_expiration"]
    return jsonify(to_dict(passenger, ignore))


''' create a passenger '''
@bp.route('/passengers', methods=['POST'])
def create_passenger():
    data = request.get_json() or {}
    required_fields = ["name", "mobile", "email", "password"]

    for field in required_fields:
        if field not in data:
            return bad_request("{} is required".format(field))

    passenger = Passenger()
    passenger.set_password(data.pop('password'))
    from_dict(data, passenger)

    try:
        db.session.add(passenger)
        db.session.commit()
    except exc.IntegrityError:
        return bad_request("passenger already registered")

    response = jsonify(to_dict(passenger, ignore=["password_hash", "token", "token_expiration"]))
    response.status_code = 201
    return response


''' update passenger details '''
@bp.route('/passengers/<int:p_id>', methods=['PUT'])
@token_required
def update_passenger(p_id):
    passenger = Passenger.query.get_or_404(p_id)
    data = request.get_json() or {}

    dont_allow = ["p_id", "mobile", "email", "token", "token_expiration"]
    for field in dont_allow:
        if field in data:
            data.pop(field)

    if "password" in data:
        password_plain = data.pop("password")
        passenger.set_password(password_plain)

    from_dict(data, passenger)
    db.session.commit()

    return jsonify(to_dict(passenger, ignore=["password_hash", "token", "token_expiration"]))


''' remove a passenger '''
@bp.route('/passengers/<int:d_id>', methods=['DELETE'])
@token_required
def remove_passenger(d_id):
    passenger = Passenger.query.get(d_id)

    if passenger:
        try:
            db.session.delete(passenger)
            db.session.commit()
        except Exception:
            return server_error("something went wrong on API side")

    return jsonify(), 204


''' get rides for passenger '''
@bp.route('/passengers/<int:p_id>/rides', methods=['GET'])
@token_required
def get_pass_rides(p_id):
    passenger = Passenger.query.get_or_404(p_id)
    rides = passenger.rides

    response = {}
    response["total_rides"] = len(rides)

    if len(rides) != 0:
        pass_rides = [to_dict(ride) for ride in rides]
        response["rides"] = pass_rides
    else:
        response["rides"] = rides

    return jsonify(response)

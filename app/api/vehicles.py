from flask import jsonify, request
from sqlalchemy import exc

from app import db
from app.api import bp
from app.models import Vehicle
from app.enums import VehicleType, FuelType
from app.utils import to_dict, from_dict
from app.api.errors import bad_request, server_error
from app.api.auth import token_required

''' retreive a particular vehicle '''
@bp.route('/vehicles/<string:v_id>', methods=['GET'])
@token_required
def get_vehicle(v_id):
    vehicle = Vehicle.query.get_or_404(v_id)
    ignore = []
    return jsonify(to_dict(vehicle, ignore))


''' add a new vehicle '''
@bp.route('/vehicles', methods=['POST'])
@token_required
def create_vehicle():
    data = request.get_json() or {}
    required_fields = ["vehicle_no", "manufacturer", "model", "vehicle_type", "fuel_type"]

    for field in required_fields:
        if field not in data:
            return bad_request("{} is required".format(field))

    if "vehicle_no" in data:
        data["v_id"] = data.pop("vehicle_no")

    if "vehicle_type" in data:
        v_type = data.pop("vehicle_type").upper()
        if hasattr(VehicleType, v_type):
            data["v_type"] = VehicleType[v_type]
        else:
            return bad_request("unknown vehicle type")

    if "fuel_type" in data:
        fuel_type = data.pop("fuel_type").upper()
        if hasattr(FuelType, fuel_type):
            data["fuel_type"] = FuelType[fuel_type]
        else:
            return bad_request("unknown fuel type")

    vehicle = Vehicle()
    from_dict(data, vehicle)

    try:
        db.session.add(vehicle)
        db.session.commit()
    except exc.IntegrityError as ie:
        print ("Exception Occurred: {}".format(ie))
        return bad_request("vehicle already exist")

    response = jsonify(to_dict(vehicle, ignore=[]))
    response.status_code = 201
    return response


''' remove a vehicle '''
@bp.route('/vehicles/<string:v_id>', methods=['DELETE'])
@token_required
def remove_vehicle(v_id):
    vehicle = Vehicle.query.get(v_id)

    if vehicle:
        try:
            db.session.delete(vehicle)
            db.session.commit()
        except Exception:
            return server_error("something went wrong on API side")

    return jsonify(), 204

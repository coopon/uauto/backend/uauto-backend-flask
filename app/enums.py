import enum

class RideStatus(enum.Enum):
    USER_REQUESTED = 0
    USER_CANCELLED = 1
    DRIVER_ACCEPTED = 2
    DRIVER_CANCELLED = 3
    TIMEDOUT = 4
    ON_GOING = 5
    COMPLETED = 6


class VehicleType(enum.Enum):
    CAR = 0
    AUTO_RICKSHAW = 1


class FuelType(enum.Enum):
    PETROL = 0
    DIESEL = 1
    LPG = 2

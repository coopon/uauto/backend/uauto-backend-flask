import base64
import os

from datetime import datetime, timedelta
from werkzeug.security import generate_password_hash, check_password_hash

from app import db
from app.enums import VehicleType, FuelType, RideStatus

class UserMixin(object):
    name = db.Column(db.String(40), index=True, nullable=False)
    mobile = db.Column(db.String(10), unique=True, index=True, nullable=False)
    password_hash = db.Column(db.String)

    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)

    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, nullable=False)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    def set_password(self, plain_text):
        self.password_hash = generate_password_hash(plain_text)

    def check_password(self, plain_text):
        return check_password_hash(self.password_hash, plain_text)


class Driver(UserMixin, db.Model):
    d_id = db.Column(db.BigInteger, primary_key=True)
    email = db.Column(db.String, unique=True, index=True, nullable=True)
    license_no = db.Column(db.String, index=True, unique=True, nullable=False)
    address = db.Column(db.Text)
    experience_year = db.Column(db.Integer)

    drives = db.relationship("Vehicle", uselist=False, back_populates="driver")
    rides = db.relationship("Ride", uselist=True, back_populates="driver")

    fleet_id = db.Column(db.BigInteger, db.ForeignKey('fleet.f_id'))
    fleet = db.relationship("Fleet", back_populates="drivers")

    @staticmethod
    def check_token(token):
        user = Driver.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

    def __repr__(self):
        return '<Driver ID: {}>'.format(self.d_id)


class Passenger(UserMixin, db.Model):
    p_id = db.Column(db.BigInteger, primary_key=True)
    email = db.Column(db.String, index=True, unique=True, nullable=False)

    rides = db.relationship("Ride", uselist=True, back_populates="passenger")

    @staticmethod
    def check_token(token):
        user = Passenger.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

    def __repr__(self):
        return '<Passenger ID: {}>'.format(self.p_id)


class Vehicle(db.Model):
    v_id = db.Column(db.String, primary_key=True)
    v_type = db.Column(db.Enum(VehicleType), index=True, nullable=False)
    manufacturer = db.Column(db.String, index=True, nullable=False)
    model = db.Column(db.String, index=True, nullable=False)
    fuel_type = db.Column(db.Enum(FuelType), nullable=False)

    driver_id = db.Column(db.BigInteger, db.ForeignKey('driver.d_id'))
    driver = db.relationship("Driver", back_populates="drives")

    fleet_id = db.Column(db.BigInteger, db.ForeignKey('fleet.f_id'))
    fleet = db.relationship("Fleet", back_populates="vehicles")

    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, nullable=False)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Vehicle ID: {}>'.format(self.v_id)


class Fleet(db.Model):
    f_id = db.Column(db.BigInteger, primary_key=True)
    fleet_for = db.Column(db.Enum(VehicleType), nullable=False)
    location = db.Column(db.String, unique=True, nullable=False)
    name = db.Column(db.String, nullable=True)
    vehicles = db.relationship("Vehicle", back_populates="fleet")
    drivers = db.relationship("Driver", back_populates="fleet")
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, nullable=False)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Fleet ID: {}>'.format(self.f_id)


class Ride(db.Model):
    r_id = db.Column(db.BigInteger, primary_key=True)

    start_location = db.Column(db.String, nullable=False)
    dest_location = db.Column(db.String, nullable=False)
    via_route = db.Column(db.Text, nullable=True)

    estimated_dist = db.Column(db.Float)
    actual_dist = db.Column(db.Float)

    estimated_cost = db.Column(db.Float)
    actual_cost = db.Column(db.Float)

    ride_gpx_path = db.Column(db.String, unique=True)
    status = db.Column(db.Enum(RideStatus), nullable=False, index=True)

    driver_id = db.Column(db.BigInteger, db.ForeignKey("driver.d_id"))
    driver = db.relationship("Driver")
    passenger_id = db.Column(db.BigInteger, db.ForeignKey("passenger.p_id"), nullable=False)
    passenger = db.relationship("Passenger")

    started_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, nullable=False)
    ended_at = db.Column(db.DateTime, index=True, nullable=True)

    def __repr__(self):
        return '<Ride ID: {}>'.format(self.r_id)

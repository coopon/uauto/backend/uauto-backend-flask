from enum import Enum
from datetime import datetime

from app.models import Driver, Passenger

def to_dict(model_instance, ignore=[]):
    response_dict = {}

    if hasattr(model_instance, '__table__'):
        for c in model_instance.__table__.columns:
            
            if c.name not in ignore:
                _value = getattr(model_instance, c.name)
                
                if isinstance(_value, Enum):
                    response_dict[c.name] = _value.name.lower()
                else:
                    response_dict[c.name] = str(getattr(model_instance, c.name))
    return response_dict


def from_dict(data, model_instance):
    for c in model_instance.__table__.columns:
        if c.name in data:
            setattr(model_instance, c.name, data[c.name])
